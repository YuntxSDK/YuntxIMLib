# iOS SDK 接口变更说明文档
### v5.6.4
1. `ECConferenceInfo.h`里`ECConferenceInfo` 对象新增参数

```
/**
 @brief Ytx_SDK_5.6.4.5 新增该字段：0 不直播 1 直播,默认值-1 不启用该字段，兼容旧版本SDK。
 */
@property (nonatomic, assign) NSUInteger autoLive;

/**
@brief autoLivePlayerUrls：后台返回视频地址 json字符串，例子：{
  "rtmp": {
    "source": "rtmp://192.168.182.117:1936/8a2af988536458c301537d7197320004/1605179402048?auth=3693c6a8ce70-0&uuid=35bd5b442aa5403696db770923b8ed49"
  },
  "hls": {
    "source": "http://192.168.182.117/hls/8a2af988536458c301537d7197320004/1605179402048.m3u8?auth=3693c6a8ce70-0&uuid=ba46748a07c342bbad49e26f85f591b8"
  },
  "flv": {
    "source": "http://192.168.182.117/8a2af988536458c301537d7197320004/1605179402048.flv?auth=3693c6a8ce70-0&uuid=b60ccfda1448461d90839b4542fe9a86"
  }
}
*/
@property (nonatomic, copy, readonly) NSString *autoLivePlayerUrls;

/**
@brief autoLiveChannelId：直播频道id
*/
@property (nonatomic, copy, readonly) NSString *autoLiveChannelId;


/**
@brief backupLinkNode：后台返回视频地址 json字符串，例子：{
  {
    "rtmp": {
      "source": {
        "public": [
          "rtmp://192.168.182.117:1936/8a2af988536458c301537d7197320004/1605179402048?auth=3693c6a8ce70-0&uuid=35bd5b442aa5403696db770923b8ed49"
        ]
      }
    },
    "hls": {
      "source": {
        "public": [
          "http://192.168.182.117/hls/8a2af988536458c301537d7197320004/1605179402048.m3u8?auth=3693c6a8ce70-0&uuid=ba46748a07c342bbad49e26f85f591b8"
        ]
      }
    },
    "flv": {
      "source": {
        "public": [
          "http://192.168.182.117/8a2af988536458c301537d7197320004/1605179402048.flv?auth=3693c6a8ce70-0&uuid=b60ccfda1448461d90839b4542fe9a86"
        ]
      }
    }
  }
*/
@property (nonatomic, copy, readonly) NSString *backupLinkNode;

```


### v5.6.3

1. `ECGroupNoticeMessage.h`里`ECGroupCreateConferenceMsg` `ECGroupJoinConferenceMessage` `ECGroupQuitConferenceMessage` `ECGroupDeleteConferenceMessage` 对象新增参数

   ```
   /**
    @brief 管理员
    */
   @property (nonatomic, copy) NSString *admin;
   
   /**
   @brief 管理员昵称
   */
   @property (nonatomic, copy) NSString *adminNickName;
   
   
   /**
    @brief member
    */
   @property (nonatomic, copy) NSString *member;
   
   /**
    @brief 昵称
    */
   @property (nonatomic, copy) NSString *nickName;
   
   /**
    @brief 1 讨论组  2普通群组
    */
   @property (nonatomic, copy) NSString *target;
   
   ```

2. `ECLiveChatRoomNoticeMessage`新增在线人数 `memberCount`;

### v5.5.3.1

1. `ECEnumDefs.h`新增枚举参数

   ```
    /** web */
    ECDeviceType_WebPage = 33,
   ```

2.  `ECGroupNoticeMessage.h`里`ECInviterMsg` `ECRemoveMemberMsg` `ECModifyGroupMsg` `ECModifyGroupMemberMsg` `ECChangeMemberRoleMsg` `ECChangeForbidMsg` `ECChangeAllForbidMsg` `ECChangeNotForbidMsg`  `ECChangeNotAllForbidMsg`对象新增参数

   ```
   /**
    @brief 管理员
    */
   @property (nonatomic, copy) NSString *admin;
   
   /**
   @brief 管理员昵称
   */
   @property (nonatomic, copy) NSString *adminNickName;
   ```

   

### v5.5.2

1. `ECGroup`新增参数

```

/**
@property
@brief isForbid 是否开启全员禁言 YES:开启 NO:关闭
*/
@property (nonatomic, assign) BOOL isForbid;

/**
@property
@brief isManage 是否仅群主可管理  0 不使用此功能 1:否 2:是
*/
@property (nonatomic, assign) NSInteger isManage;

/**
@property
@brief isAtAll 是否仅群主和管理员可@所有人 0 不使用此功能 1:否 2:是
*/
@property (nonatomic, assign) NSInteger isAtAll;

/**
@property
@brief inviteOperation 普通成员是否可拉人入群 1:是 2:否 缺省1
*/
@property (nonatomic, assign) NSInteger inviteOperation;
```

2. `ECGroupNoticeMessage`里`ECGroupMessageType`枚举新增部分值，同时新增了对应的对象

```
    /**群成员被禁言*/
    ECGroupMessageType_ForbidChange = 17,
    /**开启全员禁言*/
    ECGroupMessageType_AllForbidChange,
    /**群成员被解除禁言*/
    ECGroupMessageType_NotForbidChange,
    /**关闭全员禁言*/
    ECGroupMessageType_NotAllForbidChange,
    
    
    #pragma mark - 群禁言相关通知
/**
 * 成员禁言通知
 */
@interface ECChangeForbidMsg : ECGroupNoticeMessage

/**
 @brief 修改的成员voip号
 */
@property (nonatomic, copy) NSString *member;

/**
 @brief 昵称
 */
@property (nonatomic, copy) NSString *nickName;

/**
 @brief 1 讨论组  2普通群组
 */
@property (nonatomic, copy) NSString *target;

@end

/**
 * 全员禁言通知
 */
@interface ECChangeAllForbidMsg : ECGroupNoticeMessage

/**
 @brief 修改的成员voip号
 */
@property (nonatomic, copy) NSString *member;

/**
 @brief 昵称
 */
@property (nonatomic, copy) NSString *nickName;

/**
 @brief 1 讨论组  2普通群组
 */
@property (nonatomic, copy) NSString *target;

@end

/**
 * 解除群成员禁言通知
 */
@interface ECChangeNotForbidMsg : ECGroupNoticeMessage

/**
 @brief 修改的成员voip号
 */
@property (nonatomic, copy) NSString *member;

/**
 @brief 昵称
 */
@property (nonatomic, copy) NSString *nickName;

/**
 @brief 1 讨论组  2普通群组
 */
@property (nonatomic, copy) NSString *target;

@end

/**
 * 解除全员禁言通知
 */
@interface ECChangeNotAllForbidMsg : ECGroupNoticeMessage

/**
 @brief 修改的成员voip号
 */
@property (nonatomic, copy) NSString *member;

/**
 @brief 昵称
 */
@property (nonatomic, copy) NSString *nickName;

/**
 @brief 1 讨论组  2普通群组
 */
@property (nonatomic, copy) NSString *target;

@end

```

