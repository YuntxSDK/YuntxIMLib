//
//  ECAppMessageBody.h
//  CCPiPhoneSDK
//
//  Created by MacBook Pro on 2021/12/9.
//  Copyright © 2021 ronglian. All rights reserved.
//

#import "ECMessageBody.h"

NS_ASSUME_NONNULL_BEGIN

@interface ECAppMessageBody : ECMessageBody

/**
 @brief text 文本消息体的内部文本对象的文本
 */
@property (nonatomic, strong) NSString *text;


/**
 @brief 创建文本实例
 @param text 文本消息
 */
- (instancetype)initWithText:(NSString*)text;


@end

NS_ASSUME_NONNULL_END
