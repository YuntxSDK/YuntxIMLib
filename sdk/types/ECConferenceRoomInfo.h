//
//  ECConferenceRoomInfo.h
//  CCPiPhoneSDK
//
//  Created by 王文龙 on 2017/9/30.
//  Copyright © 2017年 ronglian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ECConferenceMemberInfo.h"

@interface ECConferenceRoomoccupy : NSObject

/**
 @brief confId 会议号
 */
@property (nonatomic, copy) NSString * confId;

/**
 @brief vStartTime 预定开始时间
 */
@property (nonatomic, copy) NSString * vStartTime;

/**
 @brief vEndTime 预定结束时间
 */
@property (nonatomic, copy) NSString * vEndTime;

/**
 @brief 预定者账号
 */
@property (nonatomic, copy) NSString * bookUserId;

/**
 @brief 预定者名称
 */
@property (nonatomic, copy) NSString * bookUserName;

/**
 @brief 预定主题
 */
@property (nonatomic, copy) NSString * bookTopic;

/**
 @brief 预定者手机号
 */
@property (nonatomic, copy) NSString * bookUserPhone;

/**
 @brief 预定类型
 */
@property (nonatomic, copy) NSString * bookType;

/**
 @brief 表示状态，0-启用，1-禁用
 */
@property (nonatomic, assign) NSInteger disable;

/**
 @brief 预定id
 */
@property (nonatomic, assign) NSInteger bookId;

@end

@interface ECConferenceRoomDeviceInfo : NSObject
/**
 @brief deviceName 硬件名称
 */
@property (nonatomic, copy) NSString * deviceName;

/**
 @brief deviceSncode 硬件编号
 */
@property (nonatomic, copy) NSString * deviceSncode;

/**
 @brief deviceType 硬件类型
 */
@property (nonatomic, copy) NSString * deviceType;

@end

@interface ECConferenceRoomInfo : NSObject
/**
 @brief 应用ID
 */
@property (nonatomic, copy) NSString * appId;

/**
 @brief confRoomId
 */
@property (nonatomic, copy) NSString * confRoomId;
/**
 @brief confRoomName
 */
@property (nonatomic, copy) NSString * confRoomName;
/**
 @brief member
 */
@property (nonatomic, retain) ECAccountInfo * member;
/**
 @brief joinState
 */
@property (nonatomic, assign) NSInteger joinState;
/**
 @brief maxMember
 */
@property (nonatomic, assign) NSInteger maxMember;
/**
 @brief createTime
 */
@property (nonatomic, copy) NSString * createTime;
/**
 @brief updateTime
 */
@property (nonatomic, copy) NSString * updateTime;

/**
 @brief 是否启用多终端登录功能，启用后成员 MemberId 字段会有变化  0表示关闭，1表示启用
 */
@property (nonatomic, assign) NSInteger multiTerminal;

/**
 @brief 楼层
 */
@property (nonatomic, copy) NSString* floor;

/**
 @brief 城市
 */
@property (nonatomic, copy) NSString* city;

/**
 @brief status 状态
 */
@property (nonatomic, assign) NSInteger status;

/**
 @brief 建筑
 */
@property (nonatomic, copy) NSString* building;


/**
 @brief 设施类型
 */
@property (nonatomic, copy) NSString* baseDevice;

/**
 @brief
 */
@property (nonatomic, copy) NSArray<ECConferenceRoomoccupy*>* occupies;


/**
 @brief
 */
@property (nonatomic, copy) NSArray<ECConferenceRoomDeviceInfo*>* deviceInfo;

@end


