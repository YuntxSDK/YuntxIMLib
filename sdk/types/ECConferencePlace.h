//
//  ECConferencePlace.h
//  CCPiPhoneSDK
//
//  Created by mac on 2021/9/27.
//  Copyright © 2021 ronglian. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ECConferencePlaceBuildings : NSObject

/**
 @brief 建筑id
 */
@property (nonatomic, assign) NSInteger bid;

/**
 @brief 建筑名称
 */
@property (nonatomic, copy) NSString* buildName;

@end

@interface ECConferencePlace : NSObject

/**
 @brief 城市id
 */
@property (nonatomic, assign) NSInteger cid;

/**
 @brief 城市名称
 */
@property (nonatomic, copy) NSString* cityName;


/**
 @brief 城市名称
 */
@property (nonatomic, copy) NSArray<ECConferencePlaceBuildings*>* buildings;


@end




NS_ASSUME_NONNULL_END
