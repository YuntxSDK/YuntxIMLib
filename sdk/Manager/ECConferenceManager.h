//
//  ECConferenceManager.h
//  CCPiPhoneSDK
//
//  Created by jiazy on 2017/2/13.
//  Copyright © 2017年 ronglian. All rights reserved.
//

#import <Foundation/Foundation.h>
#if TARGET_OS_IPHONE
#import <UIKit/UIKit.h>
#else
#import <AppKit/AppKit.h>
#endif
#import "ECError.h"
#import "ECManagerBase.h"
#import "ECConferenceInfo.h"
#import "ECConferenceCondition.h"
#import "ECConferenceMemberInfo.h"
#import "ECConferenceJoinInfo.h"
#import "ECConferenceVideoInfo.h"
#import "ECConferenceAppSettingInfo.h"
#import "ECConferenceAbstract.h"
#import "ECConferenceLiveInfo.h"
#import "ECRecordInfo.h"
#import "ECEntityConferenceInfo.h"
#import "ECConferenceSummary.h"
#import "ECConferencePlace.h"


typedef NS_ENUM(NSInteger,ECControlMediaAction) {
    /** 多状态变更 */
    ECControlMediaAction_moreChanged  = -1,
    
    /** 禁听 */
    ECControlMediaAction_CloseListen = 0,
    
    /** 可听 */
    ECControlMediaAction_OpenListen = 1,
    
    /** 禁言 */
    ECControlMediaAction_CloseSpeak = 2,
    
    /** 可说 */
    ECControlMediaAction_OpenSpeak = 3,
    
    /** 停止观看视频 */
    ECControlMediaAction_CloseLookVideo = 10,
    
    /** 观看视频 */
    ECControlMediaAction_OpenLookVideo = 11,
    
    /** 停止发布视频 */
    ECControlMediaAction_StopPublish = 12,
    
    /** 发布视频 */
    ECControlMediaAction_PublishVideo = 13,
    
    /** 停止观看屏幕共享 */
    ECControlMediaAction_CloseLookScreen =20,
    
    /** 观看屏幕共享 */
    ECControlMediaAction_OpenLookScreen = 21,
    
    /** 关闭共享 */
    ECControlMediaAction_CloseScreen = 22,
    
    /** 打开共享 */
    ECControlMediaAction_OpenScreen = 23,
    
    /** 关闭成员的白板共享收看 */
    ECControlMediaAction_StopLookBoard   = 30,
    
    /** 打开议成员的白板共享收看 */
    ECControlMediaAction_OpenLookBoard   = 31,
    
    /** 停止议成员的白板共享 */
    ECControlMediaAction_StopShareBoard  = 32,
    
    /** 开始议成员的白板共享 */
    ECControlMediaAction_OpenShareBoard  = 33,
    
    /** 禁止成员操作白板 */
    ECControlMediaAction_ForbidOperBoard = 34,
    
    /** 允许成员操作白板 */
    ECControlMediaAction_AllowOperBoard  = 35,
    
    /** 主持人变更 */
    ECControlMediaAction_HostChanged  = 50,
    
    /** 成员名字变更 */
    ECControlMediaAction_nameChanged  = 51,
    
    /** 会议拒绝接听 */
    ECControlMediaAction_rejectCalling  = 56,
    
    /** 会议摘要变化 */
    ECControlMediaAction_AbstractChanged = 76,
    
    /*主持人点名成员成为发言人*/
    ECControlApplyPublishVoiceByModerator = 77,
    
    /*主持人同意成员成为发言人*/
    ECControlAcceptPublishVoiceByModerator = 78,
    
    /*主持人拒绝成员的举手发言*/
    ECControlRejectPublishVoiceByModerator = 79,
    
    /** 主持人 取消发言人权限，即更改发言人角色为 普通成员 */
    ECControlStopPublishVoiceByModerator = 80,
    
    /** 参会人 申请发言 */
    ECControlApplyPublishVoiceBySelf = 81,
    
    /** 参会人 同意点名 */
    ECControlAcceptPublishVoiceBySelf = 82,
    
    /** 参会人 拒绝点名 */
    ECControlRejectPublishVoiceBySelf = 83,
    
    /**参会人 取消发言*/
    ECControlStopPublishVoiceBySelf = 84,
    
    /** 会议呼叫超时 */
    ECControlMediaAction_CallingTimeout = 153,
    
    /** 会议提前十五分钟开始提醒 */
    ECControlMediaAction_RemindBeforeStart = 157,
    
    /** 会议预定更新 */
    ECControlMediaAction_ConferenceRoom_Book_Update = 158,
    
    /** 会议约定取消通知 */
    ECControlMediaAction_ConferenceRoom_Book_Cancel = 159,
    
    /** 会议室禁用通知 */
    ECControlMediaAction_ConferenceRoom_Use_Forbid = 160,
    
    /** 会议室预订消息通知 */
    ECControlMediaAction_ConferenceRoom_Book = 161,
    
    /** 会议室停用 */
    ECControlMediaAction_ConferenceRoom_stop = 162

};

typedef NS_ENUM(NSUInteger,ECRecordAction) {
    
    /** 开始录音语音 */
    ECRecordAction_RecordSound = 0,
    
    /** 停止录音语音 */
    ECRecordAction_StopSound = 1,
    
    /** 开始录制摄像头视频 */
    ECControlMediaAction_RecordCameraVideo = 10,
    
    /** 停止录制摄像头视频 */
    ECControlMediaAction_StopCameraVideo = 11,
    
    /** 开始录制共享屏幕 */
    ECControlMediaAction_RecordScreenVideo =20,
    
    /** 停止录制共享屏幕 */
    ECControlMediaAction_StopScreenVideo = 21,
    
    /** 开始录制白板 */
    ECControlMediaAction_StartRecordBoard = 30,
    
    /** 停止录制白板 */
    ECControlMediaAction_StopRecordBoard = 31,
    
    /** 开始录制所有 */
    ECControlMediaAction_StartRecordAll = 40,
    
    /** 停止录制所有 */
    ECControlMediaAction_StopRecordAll = 41,
    
};

typedef NS_ENUM(NSUInteger,ECPlayAction) {
    
    /** 开始放音 */
    ECPlayAction_PlayAudio = 0,
    
    /** 停止放音 */
    ECPlayAction_StopAudio = 1,
};

/**
 * 会议管理类_V2
 * 用于创建、解散、会议成员管理等
 */
@protocol ECConferenceManager <ECManagerBase>


/**
 @brief 获取应用下的总的会议设置信息
 @param completion 执行结果回调block
 */
- (void)getConferenceAppSetting:(void(^)(ECError* error, ECConferenceAppSettingInfo *appSettingfo))completion;

/**
 @brief 获取会议室列表
 @param completion 执行结果回调block
 */
- (void)getConfroomIdListWithAccount:(ECAccountInfo *)member confId:(NSString *)confId completion:(void(^)(ECError* error, NSArray *arr))completion EC_DEPRECATED_IOS(5.2.2, 5.6.4.29,"Use - getConfroomIdListsWithAccount:confRoomId:cityId:buildingId:occupiesStatus:keyword:startTime:endTime:pageNo:pageSize:snCode:completion:");

/**
 @brief 获取会议室列表  新增
 @param ECAccountInfo  里 accountId 和 accountType 必传
 @param cityId        可选     : 城市ID(-1不选)
 @param confRoomId    可选     : 查询指定的会议室ID
 @param buildingId    可选     : 建筑ID(-1不选)
 @param occupiesStatus可选     : 状态(-1不选) 0-空闲，1-占用
 @param keyword        可选     : 模糊搜索关键字(一般为空)
 @param startTime    可选     : 查询指定时间之后，会议室的占用情况
 startTime 与 endTime 必须同时存在，才会在返回的响应中包含 占用时间（ occupies ）
 @param endTime        可选     : 查询指定时间之前，会议室的占用情况
 @param pageNo        可选     : 按页返回(-1不选)，获取第几页的内容 默认为0，0为第一页
 @param pageSize        可选     : 按页返回(-1不选)，每页返回的数据个数 默认值1000，最大值2000
 @param snCode        可选     : 会议室硬件设备代码.
 @param completion 执行结果回调block
 */
- (void)getConfroomIdListsWithAccount:(ECAccountInfo*)member
                           confRoomId:(NSString*)confRoomId
                              cityId:(int)cityId
                          buildingId:(int)buildingId
                      occupiesStatus:(int)occupiesStatus
                             keyword:(NSString*)keyword
                           startTime:(NSString*)startTime
                             endTime:(NSString*)endTime
                              pageNo:(int)pageNo
                            pageSize:(int)pageSize
                               snCode:(NSString*)snCode
                          completion:(void(^)(ECError* error, NSArray *array))completion;

/**
 @brief 创建会议
 @param conferenceInfo 会议类
 @param bookIds    会议室预定ID列表 如:[1,2,3,5]
 @param comment    会议内容
 
 @param bookInfos  会议室预订信息列表.{"bookInfos":[{
                                                        "confRoomId":"string 会议室Id",
                                                        "startTime":"string 开始时间",
                                                        "endTime":"string 结束时间",
                                                        "confRoomType":"string 会议室类型",
                                                        "confRoomTopic":"string 会议室主题",
                                                        "userPhone":"string 预订者手机号"
                                                    }]
                                                }

 
 @param completion 执行结果回调block
 */
- (void)createConference:(ECConferenceInfo*)conferenceInfo
                 bookIds:(NSArray*)bookIds
                 comment:(NSString*)comment
               bookInfos:(NSString*)bookInfos
              completion:(void(^)(ECError* error, ECConferenceInfo*conferenceInfo))completion;


/**
 @brief 创建会议
 @param conferenceInfo 会议类
 @param bookIds    会议室预定ID列表 如:[1,2,3,5]
 @param comment    会议内容
 @param completion 执行结果回调block
 */
- (void)createConference:(ECConferenceInfo*)conferenceInfo
                 bookIds:(NSArray*)bookIds
                 comment:(NSString*)comment
              completion:(void(^)(ECError* error, ECConferenceInfo*conferenceInfo))completion;

/**
 @brief 创建会议
 @param conferenceInfo 会议类
 @param completion 执行结果回调block
 */
- (void)createConference:(ECConferenceInfo*)conferenceInfo completion:(void(^)(ECError* error, ECConferenceInfo*conferenceInfo))completion;

/**
 @brief 删除会议
 @param confId 会议ID
 @param bookIds    会议室预定ID列表 如:[1,2,3,5]
 @param completion 执行结果回调block
 */
- (void)deleteConference:(NSString*)confId
                 bookIds:(NSArray*)bookIds
              completion:(void(^)(ECError* error))completion;

/**
 @brief 删除会议
 @param confId 会议ID
 @param completion 执行结果回调block
 */
- (void)deleteConference:(NSString*)confId completion:(void(^)(ECError* error))completion;

/**
 @brief 更新会议信息
 @param conferenceInfo 会议类,注意字段exJsonContent 
 @param completion 执行结果回调block
 */
- (void)updateConference:(ECConferenceInfo*)conferenceInfo completion:(void(^)(ECError* error))completion;

/**
 @brief 更新历史会议信息
 @param conferenceInfo 会议类
 @param completion 执行结果回调block
 */
- (void)updateHistoryConference:(ECConferenceInfo*)conferenceInfo completion:(void(^)(ECError* error))completion;

/**
 @brief 更新历史会议信息
 @param conferenceInfo 会议类
 @param appData app自定义信息
 @param completion 执行结果回调block
 */
- (void)updateHistoryConference:(ECConferenceInfo*)conferenceInfo withAppData:(NSString *)appData completion:(void(^)(ECError* error))completion;

/**
 @brief 获取摘要列表
 @param conferenceId 会议id
 @param isHistoryConf 是否历史会议
 @param CGPage 分页
 @param completion 执行结果回调block
 */
- (void)getConferenceAbstractList:(NSString*)conferenceId historyConf:(BOOL)isHistoryConf page:(CGPage)page completion:(void(^)(ECError* error, NSArray* abstractList))completion;

/**
 @brief 更新摘要信息
 @param conferenceId 会议id
 @param isHistoryConf 是否历史会议
 @param ECConferenceAbstract 摘要内容（摘要ID传空时为创建新的摘要）
 @param completion 执行结果回调block
 */
- (void)updateConferenceAbstract:(NSString*)conferenceId historyConf:(BOOL)isHistoryConf abstract:(ECConferenceAbstract*)abstract completion:(void(^)(ECError* error, ECConferenceAbstract* abstract))completion;

/**
 @brief 删除摘要
 @param conferenceId 会议id
 @param isHistoryConf 是否历史会议
 @param abstractIds 摘要ID
 @param completion 执行结果回调block
 */
- (void)deleteConferenceAbstract:(NSString*)conferenceId historyConf:(BOOL)isHistoryConf abstractIds:(NSArray*)abstractIds completion:(void(^)(ECError* error))completion;

/**
 @brief 发送透传信息
 @param cmdData 透传字段
 @param members ECAccountInfo数组 控制成员列表
 @param isAllMember 是否全部成员
 @param confId 会议ID
 @param action           : (可选)10-打开全员签到，11-关闭全员签到
 @param completion 执行结果回调block
 */
- (void)conferenceSendCmd:(NSString *)cmdData toMembers:(NSArray*)members isAll:(int)isAllMember ofConference:(NSString*)confId action:(int)action completion:(void(^)(ECError* error))completion;


/**
 @brief 获取会议信息
 @param confId 会议ID
 @param completion 执行结果回调block
 */
- (void)getConference:(NSString*)confId completion:(void(^)(ECError* error, ECConferenceInfo*conferenceInfo))completion;

/**
 @brief 获取会议信息 新增
 @param confId 会议ID 必选
 @param historyConf  : (可选) 是否历史会议 -1 不选，默认为0仅查询未结束的会议
         0 : 当前会议，仅查询未结束的会议（包含预约和正在进行中的会议）
         1 : 历史会议，仅查询已结束的会议（包含已经结束和已经取消的会议）
         2 : 同时查询 未结束会议 和 已结束会议

 @param completion 执行结果回调block
 */
- (void)getConference:(NSString*)confId
          historyConf:(NSInteger)historyConf
           completion:(void(^)(ECError* error, ECConferenceInfo*conferenceInfo))completion;


/**
 @brief 获取会议列表
 @param condition 筛选条件
 @param page 分页信息
 @param completion 执行结果回调block
 */
- (void)getConferenceListWithCondition:(ECConferenceCondition*)condition page:(CGPage)page ofMember:(ECAccountInfo*)member completion:(void(^)(ECError* error, NSArray* conferenceList))completion;

/**
 @brief 获取会议列表
 @param condition 筛选条件
 @param confRoomId 会议室id
 @param page 分页信息
 @param completion 执行结果回调block
 */
- (void)getConferenceListWithCondition:(ECConferenceCondition*)condition confRoomId:(NSString *)confRoomId page:(CGPage)page ofMember:(ECAccountInfo*)member completion:(void(^)(ECError* error, NSArray* conferenceList))completion;

/**
 @brief 获取会议列表  新增
 @param condition 筛选条件
 @param confRoomId 会议室id
 @param memberInfo 成员信息
 @param page 分页信息 pageNo 按页返回，默认为0,0为第一页, size每页返回的会议条数，默认20，最大2000
 @param completion 执行结果回调block 其中字典里 totalCount:总条数;processCount:进行中的会议条数;reserverCount:预约的会议条数
 */
- (void)getConferenceListWithCondition:(ECConferenceCondition*)condition confRoomId:(NSString *)confRoomId page:(CGPage)page member:(ECAccountInfo*)member  completion:(void(^)(ECError* error, NSArray* conferenceList,NSDictionary* countDataInfo))completion;

/**
 @brief 获取历史会议列表
 @param condition 筛选条件
 @param page 分页信息
 @param completion 执行结果回调block
 */
- (void)getHistoryConferenceListWithCondition:(ECConferenceCondition*)condition page:(CGPage)page completion:(void(^)(ECError* error, NSArray* conferenceList))completion;

/**
 @brief 获取历史会议列表
 @param condition 筛选条件
 @param confRoomId 会议室id
 @param page 分页信息
 @param completion 执行结果回调block
 */
- (void)getHistoryConferenceListWithCondition:(ECConferenceCondition*)condition confRoomId:(NSString *)confRoomId page:(CGPage)page completion:(void(^)(ECError* error, NSArray* conferenceList))completion;

/**
 @brief 获取历史会议列表  新增
 @param page 分页信息 pageNo 按页返回，默认为0,0为第一页, size每页返回的会议条数，默认20，最大2000
 @param condition 筛选条件
 @param confRoomId 会议室id
 @param completion 执行结果回调block 其中字典里 totalCount:总条数;processCount:进行中的会议条数;reserverCount:预约的会议条数
 */
- (void)getHistoryConferenceListPage:(CGPage)page  condition:(ECConferenceCondition*)condition confRoomId:(NSString *)confRoomId completion:(void(^)(ECError* error, NSArray* conferenceList,NSDictionary* countDataInfo))completion ;

/**
 @brief 锁定会议
 @param confId 会议ID
 @param lockType 0 锁定，1 解锁，2 锁定白板发起，3 解锁，4 锁定白板标注，5 解锁
 @param completion 执行结果回调block
 */
- (void)lockConference:(NSString*)confId lockType:(int)lockType completion:(void(^)(ECError* error))completion;

/**
 @brief 加入会议
 @param joinInfo 加入条件
 @param completion 执行结果回调block
 */
- (void)joinConferenceWith:(ECConferenceJoinInfo*)joinInfo completion:(void(^)(ECError* error, ECConferenceInfo*conferenceInfo))completion;

/**
 @brief 退出会议
 @param confId 会议ID
 @param completion 执行结果回调block
 */
- (void)quitConference:(NSString*)confId completion:(void(^)(ECError* error))completion;

/**
 @brief 更换成员信息
 @param memberInfo 成员信息
 @param confId 会议ID
 @param completion 执行结果回调block
 */
- (void)updateMember:(ECConferenceMemberInfo*)memberInfo ofConference:(NSString*)confId completion:(void(^)(ECError* error))completion;

/**
 @brief 获取成员信息
 @param member 账号信息
 @param confId 会议ID
 @param completion 执行结果回调block
 */
- (void)getMember:(ECAccountInfo*)member ofConference:(NSString*)confId completion:(void(^)(ECError* error, ECConferenceMemberInfo* memberInfo))completion;

/**
 @brief 获取成员列表
 @param confId 会议ID
 @param page 分页信息
 @param completion 执行结果回调block
 */
- (void)getMemberListOfConference:(NSString*)confId page:(CGPage)page completion:(void(^)(ECError* error, NSArray* members))completion;

/**
 @brief 获取会议中的成员与会记录
 @param accountInfo 相关成员记录 如果为nil，获取所有成员与会记录
 @param confId 会议ID
 @param page 分页信息
 @param completion 执行结果回调block
 */
- (void)getMemberRecord:(ECAccountInfo*)accountInfo ofConference:(NSString*)confId page:(CGPage)page completion:(void(^)(ECError* error, NSArray* membersRecord))completion;

/**
 @brief 获取会议中的成员与会记录
 @param accountInfo 相关成员记录 如果为nil，获取所有成员与会记录
 @param confId 会议ID
 @param page 分页信息
 @param duplicate 返回的数据是否包含重复成员 0:返回去除的结果，每个成员仅返回一条记录 1:返回所有成员，同一成员可能入会多次，结果包含重复
 @param completion 执行结果回调block
 */
- (void)getMemberRecord:(ECAccountInfo*)accountInfo ofConference:(NSString*)confId page:(CGPage)page duplicate:(int)duplicate completion:(void(^)(ECError* error, NSArray* membersRecord))completion;

/**
 @brief 邀请加入会议
 @param inviteMembers ECAccountInfo数组 邀请的成员
 @param confId 会议ID
 @param callImmediately 是否立即发起呼叫 对于自定义账号，1表示给用户显示呼叫页面，并设置超时时间1分钟 对于电话号码（或关联电话）账号，1表示立即给cm发呼叫命令 0表示仅在会议中增加成员（一般用户预约会议开始前增加成员）
 @param appData 预留
 @param completion 执行结果回调block
 */
- (void)inviteMembers:(NSArray*)inviteMembers inConference:(NSString*)confId callImmediately:(int)callImmediately appData:(NSString*)appData completion:(void(^)(ECError* error))completion;

/**
 @brief 邀请加入会议
 @param inviteMembers ECAccountInfo数组 邀请的成员
 @param confId 会议ID
 @param callImmediately 是否立即发起呼叫 对于自定义账号，1表示给用户显示呼叫页面，并设置超时时间1分钟 对于电话号码（或关联电话）账号，1表示立即给cm发呼叫命令 0表示仅在会议中增加成员（一般用户预约会议开始前增加成员）
 @param appData 预留
 @param seqCall 默认值0 顺振铃 1 同振铃 多端情况下是否同振铃,针对含有用户顺呼列表时有效.
 @param completion 执行结果回调block
 */
- (void)inviteMembers:(NSArray*)inviteMembers inConference:(NSString*)confId callImmediately:(int)callImmediately appData:(NSString*)appData seqCall:(NSInteger)seqCall completion:(void(^)(ECError* error))completion;

/**
 @brief 拒绝会议邀请
 @param invitationId 邀请的id
 @param confId 会议ID
 @param completion 执行结果回调block
 */
- (void)rejectInvitation:(NSString*)invitationId cause:(NSString*)cause ofConference:(NSString*)confId completion:(void(^)(ECError* error))completion;
/**
 @brief 踢出成员
 @param kickMembers ECAccountInfo数组 踢出的成员
 @param confId 会议ID
 @param appData 预留
 @param completion 执行结果回调block
 */
- (void)kickMembers:(NSArray*)kickMembers outConference:(NSString*)confId appData:(NSString*)appData completion:(void(^)(ECError* error))completion;

/**
 @brief 设置成员角色
 @param member 相关设置信息
 @param confId 会议ID
 @param completion 执行结果回调block
 */
- (void)setMemberRole:(ECAccountInfo*)member ofConference:(NSString*)confId completion:(void(^)(ECError* error))completion;

/**
 @brief 媒体控制
 @param action 控制动作
 @param members ECAccountInfo数组 控制成员列表
 @param isAllMember 是否全部成员
 @param confId 会议ID
 @param completion 执行结果回调block
 */
- (void)controlMedia:(ECControlMediaAction)action toMembers:(NSArray*)members isAll:(int)isAllMember ofConference:(NSString*)confId completion:(void(^)(ECError* error))completion;

/**
 @brief 媒体控制

 @param action 控制动作
 @param changeable 是否不可被目标成员更改，0：否，1：是
 @param members 数组 控制成员列表， id
 @param isAllMember 是否全部成员
 @param confId 会议ID
 @param completion 执行结果回调block
 */
- (void)controlMedia:(ECControlMediaAction)action isChangable:(int)changeable toMembers:(NSArray*)members isAll:(int)isAllMember ofConference:(NSString*)confId  completion:(void(^)(ECError* error))completion;

/**
 @brief 会议音频是否sdk自动控制   1 sdk收到会控通知会自动控制音频 0 sdk不会控制音频相关

 @param isAuto 是否自动控制
 */
- (NSInteger)setConferenceAutoVoiceControl:(BOOL)isAuto;

/**
 @brief 会议媒体是否sdk自动控制   1 sdk收到会控通知会自动控制媒体 0 sdk不会控制媒体相关

 @param isAuto 是否自动控制
 */
- (NSInteger)setConferenceAutoMediaControl:(BOOL)isAuto;

/**
 @brief 会议录制
 @param action 录制控制
 @param confId 会议ID
 @param completion 执行结果回调block
 */
- (void)record:(ECRecordAction)action conference:(NSString*)confId completion:(void(^)(ECError* error))completion;

/**
 @brief 发布音频
 @param confId 会议ID
 @param exclusively 仅用于实时对讲功能。
 1 表示控麦，此时仅允许会中有一个人发布语音，如果已经有人发布语音，此接口调用会返回错误
 0 表示发布语音，不考虑其他人语音发布状态
 @param completion 执行结果回调block
 */
- (void)publishVoiceInConference:(NSString*)confId exclusively:(int)exclusively completion:(void(^)(ECError* error))completion;

/**
 @brief 取消发布
 @param confId 会议ID
 @param exclusively 仅用于实时对讲功能。
 1 表示控麦，此时仅允许会中有一个人发布语音，如果已经有人发布语音，此接口调用会返回错误
 0 表示发布语音，不考虑其他人语音发布状态
 @param completion 执行结果回调block
 */
- (void)stopVoiceInConference:(NSString*)confId exclusively:(int)exclusively completion:(void(^)(ECError* error))completion;

/**
 @brief 发布视频
 @param confId 会议ID
 @param completion 执行结果回调block
 */
- (void)publishVideoInConference:(NSString*)confId completion:(void(^)(ECError* error))completion;

/**
 @brief 取消发布
 @param confId 会议ID
 @param completion 执行结果回调block
 */
- (void)cancelVideoInConference:(NSString*)confId completion:(void(^)(ECError* error))completion;

/**
 @brief 共享屏幕
 @param confId 会议ID
 @param completion 执行结果回调block
 */
- (void)shareScreenInConference:(NSString*)confId completion:(void(^)(ECError* error))completion
EC_DEPRECATED_IOS(5.2.2, 5.6.4.29,"Use - hareScreenInConference:shareType:completion:");

/**
 @brief 共享屏幕
 @param confId 会议ID
 @param shareType 共享类型(-1 不选) 0-共享桌面；1-共享应用.
 @param completion 执行结果回调block
 */
- (void)shareScreenInConference:(NSString*)confId shareType:(NSInteger)shareType completion:(void(^)(ECError* error))completion ;


/**
 @brief 停止共享屏幕
 @param confId 会议ID
 @param completion 执行结果回调block
 */
- (void)stopScreenInConference:(NSString*)confId completion:(void(^)(ECError* error))completion;

/**
 @brief 发布共享
 @param confId 会议ID
 @param shareInfo 共享的信息
 @param completion 执行结果回调block
 */
- (void)startWhiteboardSharing:(NSString*)confId shareInfo:(NSString *)shareInfo completion:(void(^)(ECError* error))completion;

/**
 @brief 停止共享
 @param confId 会议ID
 @param shareInfo 共享的信息
 @param completion 执行结果回调block
 */
- (void)stopWhiteboardSharing:(NSString*)confId shareInfo:(NSString *)shareInfo completion:(void(^)(ECError* error))completion;

/**
 @brief 请求成员视频
 @param videoInfo 视频信息
 @param completion 执行结果回调block
 */
- (void)requestMemberVideoWith:(ECConferenceVideoInfo*)videoInfo completion:(void(^)(ECError* error))completion;

/**
 @brief 停止成员视频
 @param videoInfo 视频信息
 @param completion 执行结果回调block
 */
- (void)stopMemberVideoWith:(ECConferenceVideoInfo*)videoInfo completion:(void(^)(ECError* error))completion;

/**
 @brief 重置显示View
 @param videoInfo 视频信息
 */
- (int)resetMemberVideoWith:(ECConferenceVideoInfo*)videoInfo;

/**
 @brief 重置本地预览显示View
 */
#if TARGET_OS_IPHONE
- (int)resetLocalVideoWithConfId:(NSString *)confId remoteView:(UIView *)remoteView localView:(UIView *)localView;
#elif TARGET_OS_MAC
- (int)resetLocalVideoWithConfId:(NSString *)confId remoteView:(NSView *)remoteView localView:(NSView *)localView;
#endif

//新增窗口绑定接口
/**
 @brief 绑定视频渲染窗口
 @param videoInfo 视频窗口信息
 @param completion 执行结果回调block
 */
- (int)attachMemberRenderWith:(ECConferenceVideoInfo*)videoInfo;

/**
 @brief 解绑视频某个渲染窗口
 @param videoInfo 视频窗口信息
 @param completion 执行结果回调block
 */
- (int)detachMemberRenderWith:(ECConferenceVideoInfo*)videoInfo;

/**
 @brief 解绑视频所有渲染窗口
 @param videoInfo 视频信息
 @param completion 执行结果回调block
 */
- (int)removeAllMemberRenderWith:(ECConferenceVideoInfo*)videoInfo;

/**
 @brief 获取所有视频订阅信息
 @param completion 执行结果回调block
 */
- (NSArray <ECConferenceVideoInfo *>*)getAllSubscribeVideoInfo;

/**
 @brief 获取通话中的视频统计报告
 reportsJsonOut:  统计数据
 {
   "AudioStats" : {
     "Receive" : [
       {
         "codec_name" : "opus",
         "fraction_lost" : 0
       }
     ],
     "SendStream" : [
       {
         "codec_name" : "opus",
         "fraction_lost" : 0
       }
     ]
   },
   "TotalStats" : {
     "Rtt_ms" : 9,
     "Estimated_received_kbps" : 1489,
     "Estimated_send_kbps" : 560
   },
   "VideoStats" : [
     {
       "SendStreams" : [
         {
           "height" : 360,
           "total_kbps" : 0,
           "channel_id" : 1,
           "codec_name" : "H264",
           "encoded_kbps" : 0,
           "fraction_lost" : 0,
           "width" : 480,
           "framerate" : 0
         }
       ]
     },
     {
       "ReceicverStreams" : [
         {
           "height" : 480,
           "channel_id" : 2,
           "codec_name" : "H264",
           "fraction_lost" : 0,
           "width" : 640,
           "received_kbps" : 64,
           "framerate" : 2
         }
       ]
     },
     {
       "ReceicverStreams" : [
         {
           "height" : 362,
           "channel_id" : 3,
           "codec_name" : "H264",
           "fraction_lost" : 0,
           "width" : 640,
           "received_kbps" : 0,
           "framerate" : 2
         }
       ]
     }
   ]
 }
 */
- (int)getStatsReportsCompletion:(void(^)(NSString* reportsJsonOut))completion;

/**
 @brief 发送DTMF
 @param confID 会议id
 @param dtmf 键值
 @return 0:成功  非0:失败
 */
- (NSInteger)sendDTMFWithConfID:(NSString *)confID dtmf:(NSString *)dtmf;

#pragma mark -- 5.4.3.4

/**
 @brief 网络抓包

 @param confId 会议id
 @param memberId 成员id
 @param mediaType 媒体类型 （ECMediaType_Video 视频、ECMediaType_Share 共享）
 @param fileName 抓包写入的文件路径
 @return 是否成功 0：成功； 非0失败
 */
- (int)conferenceStartMemberRtpDump:(NSString *)confId member:(NSString *)memberId mediaType:(ECMediaType)mediaType fileName:(NSString *)fileName;


/**
 @brief 停止抓包

 @param confId 会议id
 @param memberId 成员id
 @param mediaType 媒体类型
 @return 是否成功 0：成功； 非0失败
 */
- (int)conferenceStopMemberRtpDump:(NSString *)confId member:(NSString *)memberId   mediaType:(ECMediaType)mediaType;

/**
 @brief 设置声音最大最小参会成员显示的时间间隔

 @param timeInterVal 时间间隔
 @return 设置是否成功
 */
- (int)conferenceParticipantCallbackTimeInterVal:(NSInteger)timeInterVal;

#pragma mark === 5.4.4.4

/**
 @brief 开始会议直播

 @param conference 直播会议id
 @param userId 用户id
 @param record 是否录像
 @param isAuto 混屏切换模式，YES 自动， NO 手动
 @param completion 完成回调
 @return 请求发送成功/失败
 */
- (int)conferenceStartLive:(ECConferenceStartLiveRequest *)requestInfo completion:(void(^)(ECError* error, ECConferenceLiveUrlInfo *liveUrl))completion;

/**
 @brief 停止会议直播

 @param conferenceId 直播会议id
 @param userId 用户id
 @param completion 完成回调
 @return 请求发送成功/失败
 */
- (int)conferenceStopLive:(NSString *)conferenceId completion:(void(^)(ECError* error))completion;

/**
 @brief 获取会议直播url

 @param conferenceId 会议id
 @param userId 用户id
 @param completion 完成回调
 @return 请求发送成功/失败
 */
- (int)conferenceGetPlayUrl:(NSString *)conferenceId completion:(void(^)(ECError* error, ECConferenceLiveUrlInfo *liveUrl))completion;

/**
 @brief 会议成员直播切换

 @param conferenceId 会议id
 @param userId 用户id
 @param members 会议成员(ECAccount)信息数组，accountId、accountType必选
 @param completion 完成回调
 @return 请求发送结果
 */
- (int)conferenceSwitchMember:(NSString *)conferenceId members:(NSArray *)members   completion:(void(^)(ECError* error))completion;

#pragma mark === 5.4.6

/**
 @brief 设置呼叫类型

 @param confCallType 类型， 设置会议呼叫类型，比如采用思科双流还是POLYCOM双流呼叫,目前传递6或者是7，其他暂时不支持
 @return 设置回调
 */
- (int)setConferenceCallType:(int)confCallType;

#pragma mark ==== 5.4.10

/**
 获取会议录制文件列表

 @param confId 会议ID
 @param isHistory 指明当前会议类型 0:当前会议 1:历史会议
 @param page 分页
 @param completion 完成回调, ECRecordInfo 数组
 */
- (void)conferenceRecordList:(NSString *)confId isHistory:(int)isHistory page:(CGPage)page completion:(void(^)(ECError* error, NSArray *recordLisr))completion;

/**
 获取会议纪要列表

 @param confId 会议id
 @param page 分页
 @param completion 完成回调， ECConferenceSummary数组
 */
- (void)conferenceSummaryList:(NSString *)confId page:(CGPage)page completion:(void(^)(ECError* error, NSArray *summaryLisr))completion;


/**
 延长会议时间
 add by lxj

 @param confId (必选)会议ID
 @param tryMinDuration 可选 延长的最少时间，单位：分钟 如果无法延长到该最少时间，更新失败
 @param tryMaxDuration 可选 延长的最大时间，单位：分钟 如果能满足延长至该最大时间，会议将延长该最大时间。如果不能满足延长该值，服务器延长至可延长的最大时间
 @param completion 完成回调
 */
- (void)extendDurationInConference:(NSString *)confId tryMinDuration:(int)tryMinDuration tryMaxDuration:(int)tryMaxDuration completion:(void(^)(ECError* error))completion;


/**
 会议通用接口

 @param path 接口路径
 @param jsonString json字符串
 @param completion 回调
 */
- (void)commonConferenceByPath:(NSString *)path jsonString:(NSString *)jsonString completion:(void(^)(ECError *error,NSString *jsonString))completion;

/**
 查询建筑信息
 
 @param  cityId        可选    : 城市ID  -1 表示不选
 @param buildingId    可选    : 建筑ID -1 表示不选
 @param keyword        可选    : 模糊搜索关键字(一般为空)
 @param pageNo        可选     : 按页返回(-1不选)，获取第几页的内容 默认为0，0为第一页.
 @param pageSize       可选    : 按页返回(-1不选)，每页返回的数据个数 默认值1000，最大值2000.
 @param completion 完成回调, ECRecordInfo 数组
 */
- (void)searchConferenceRoomPlaceWithCityId:(int)cityId
                                 buildingId:(int)buildingId
                                    keyword:(NSString*)keyword
                                     pageNo:(int)pageNo
                                   pageSize:(int)pageSize
                                 completion:(void(^)(ECError* error, NSArray <ECConferencePlace*>*places))completion;
/**
 预定会议室
 @param confRoomId    必选     : 会议室ID
 @param inviteMembers    可选:邀请成员信息数组,其中
 ECAccountInfo 的属性accountType、userName、memberId必传
 @param startTime    必选     : 预约开始时间(预定有效) "2021-09-24 18:00:00"
 @param endTime        必选     : 预约结束时间(预定有效) "2021-09-24 18:30:00"
 @param userPhone  可选  用户手机号
 @param userName      可选     : 预定者用户名
 @param confRoomType    必选     : 会议室类型
 @param confRoomTopic    必选     : 预定会议室议题
 @param completion 回调
 */
- (void)bookConferenceRoomWithConfRoomId:(NSString*)confRoomId
                            confRoomType:(NSString*)confRoomType
                           confRoomTopic:(NSString*)confRoomTopic
                           inviteMembers:(NSArray<ECAccountInfo*>*)inviteMembers
                               userPhone:(NSString*)userPhone
                                userName:(NSString*)userName
                               startTime:(NSString*)startTime
                               endTime:(NSString*)endTime
                            completion:(void(^)(ECError *error,NSString *jsonString))completion;

/**
 取消会议室预定
 @param bookIds    会议室预定ID列表 如[1,2,3,5]
 @param completion 回调
 */
- (void)deleteConferenceRoomBookWithBookIds:(NSArray*)bookIds
                                    completion:(void(^)(ECError *error,NSString *jsonSt·ring))completion;

/**
 更新会议室预定
 @param bookId    必选     : bookId
 @param confRoomType    必选     : 会议室类型
 @param confRoomTopic    必选     : 预定会议室议题
 @param inviteMembers    可选:邀请成员信息数组,其中
 ECAccountInfo 的属性accountType、userName、memberId必传
 @param completion 回调
 */
- (void)updateConferenceRoomBookWithbookId:(int)bookId
                              confRoomType:(NSString*)confRoomType
                             confRoomTopic:(NSString*)confRoomTopic
                             inviteMembers:(NSArray<ECAccountInfo*>*)inviteMembers
                                    completion:(void(^)(ECError *error,NSString *jsonString))completion;
/**
 获取会议室类型
 @param completion 回调
 */
- (void)getConferenceRoomTypeCompletion:(void(^)(ECError *error,NSString *jsonString))completion;

/**
 根据投屏码获取设备
 @param screenCode    必选     : 投屏码
 @param completion 回调
 */
- (void)getConferenceDeviceFromCode:(NSString*)screenCode
                         Completion:(void(^)(ECError *error,NSString *jsonString))completion;

/**
 获取电话入会文案中的电话号码
 @param completion 回调
 */
- (void)conferenceSharePhoneCompletion:(void(^)(ECError *error,NSString *jsonString))completion;

/**
 获取自己的会议室预定列表
 @param startTime     必选     : 开始时间
 @param endTime       必选     : 结束时间
 @param completion 回调
 */
- (void)getConferenceRoomBookListWithStartTime:(NSString*)startTime
                                       endTime:(NSString*)endTime
                         Completion:(void(^)(ECError *error,NSString *jsonString))completion;

/**
 获取会议室预定设置信息(行政人员账号判断、获取当前系统时间等)
 @param completion 回调
 */
- (void)getConferenceRoomBookSettingCompletion:(void(^)(ECError *error,NSString *jsonString))completion;

/**
 会议开启屏幕共享
 @param confId    会议id
 @param rotate 旋转角度
 @param timestamp 时间戳
 @param completion 回调
 */
- (int)conferenceCaptureScreenDataWith:(NSString*)confId data:(unsigned char *)data width:(int)width height:(int)height timestamp:(long)timestamp rotate:(ECRotate)rotate;
@end

